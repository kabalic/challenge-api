﻿using Mi9WebApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Mi9WebApi.Filters
{
    public class FilterRequest
    {
        public static RequestResponse FilterJsonString(Root data)
        {
            var responseData = data.Payload.Where(x => x.Drm == true && x.EpisodeCount > 0);

            var response = responseData.Select(r => new Response { Image = r.Image.ShowImage, Slug = r.Slug, Title = r.Title }).ToArray();

            var rootresponse = new RequestResponse();

            rootresponse.Response= response.ToArray();

            return rootresponse;
        }
    }
}