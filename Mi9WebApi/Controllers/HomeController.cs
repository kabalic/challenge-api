﻿using Mi9WebApi.Filters;
using Mi9WebApi.Models;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace Mi9WebApi.Controllers
{
    public class HomeController : ApiController
    {
        public HttpResponseMessage Get([FromBody]string jsonBody)
        {
            return Request.CreateResponse(HttpStatusCode.OK, "No Data Requested");
        }

        public HttpResponseMessage PostShows()
        {
            try
            {
                string jsonstring = Request.Content.ReadAsStringAsync().Result;

                Root data = JsonConvert.DeserializeObject<Root>(jsonstring);

                var response = FilterRequest.FilterJsonString(data);

                if (response == null)
                {
                    return Request.CreateErrorResponse(HttpStatusCode.NotFound, "No records for the submitted json could not be retrieved");
                }

                return Request.CreateResponse(HttpStatusCode.OK, response);

            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Error: Could not decode request: JSON parsing failed");
            }
        }
    }
}
