﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Mi9WebApi.Models
{
    [DataContract]
    public class Image
    {
        [DataMember]
        public string ShowImage { get; set; }
    }
}