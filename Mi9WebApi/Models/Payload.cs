﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Mi9WebApi.Models
{
    public class Payload
    {
        [DataMember]
        public string Country { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public bool Drm { get; set; }
        [DataMember]
        public int EpisodeCount { get; set; }
        [DataMember]
        public string Genre { get; set; }
        [DataMember]
        public Image Image { get; set; }
        [DataMember]
        public string language { get; set; }
        [DataMember]
        public Episode NextEpisode { get; set; }
        [DataMember]
        public string primaryColour { get; set; }
        public Season[] Seasons { get; set; }
        [DataMember]
        public string Slug { get; set; }
        [DataMember]
        public string Title { get; set; }
    }
}