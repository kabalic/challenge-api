﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Mi9WebApi.Models
{
    [DataContract]
    public class Episode
    {
        [DataMember]
        public string Channel { get; set; }
        [DataMember]
        public string channelLogo { get; set; }
        [DataMember]
        public DateTime Date { get; set; }
        [DataMember]
        public string html { get; set; }
        [DataMember]
        public string url { get; set; }
    }
}